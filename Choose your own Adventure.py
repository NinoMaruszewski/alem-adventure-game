"""
Alem Snyder
Computer science
Choose your own Adventure"""

duck = "!!Quack!!" # duck is not a string!!!!!!!!!!

from random import randint # Chaos is necessary
def D(num):
    return randint(1, num)
# define some variables
position = 0
yes = ("Y", "YES", "")
no = ("NO","N")

# redo this
goblins4_L1= True
goblins4_L2= True
goblins6 = True 
goblins8 = True
goblins10 = True

def Damage(armor, weaponD):
    if weaponD == "Shortswords":
        d = 0
        if D(20) > armor:
            d = D(6)
            print("Good hit")
        else:
            print("You missed but have another try.")
        if D(20) > armor:
            d += D(6)
            print("Good hit")
        else:
            print("You missed with you second shotsword")
    else:
        if D(20) > armor:
            d = D(weaponD)
            print("That's bound to do something!")
        else:
            print("big miss")
            d = 0
    return d
def DamageE(armor, weaponD):
    if D(20) > armor:
        d = D(weaponD)
        print("That's going to hurt.")
    else:
        print("An outstanding dodge!")
        d = 0
    return d

def fightGoblins(numGob, stats, boss = False):
    enemies=[]
    for Gob in range(numGob):
        enemies.append({"name": "Goblin "+str(Gob+1), "health": 6,"attack": 4,"armor":12})
    if boss:
        enemies.append({"name": "The Goblin King", "health": 30,"attack": 8,"armor":14})
    print("There are "+str(numGob)+" goblins.")
    if boss:
        print("There is also the King Goblin")
    #stats["health"]
    """if "6" in equipment:
        health = 80
    else:
        health = 60"""
    while len(enemies) >0:
        if stats["health"] <= 0:
            print("You no longer possess the capacity to live. You might make a good stew.\nThe Goblins would like that.")
            return 1
        t = 0
        print("You have "+str(stats["health"])+" health")
        while True:
            decision=input("Do you stay in the cave?[y]/n\n")
            if decision.upper() in no:
                print("Running away is better than death, but you couldn't even hold your own against goblins.")
                return 2
            elif decision.upper() in yes:
                while True:
                    if len(enemies) == 1:
                        NG = "is one"
                    else:
                        NG = "are "+str(len(enemies))
                    decision=input("Which Goblin would you like to hit, there "+NG+". (1-"+str(len(enemies))+")\n")
                    try:
                        dec = int(decision)
                        if dec <= len(enemies):
                            enemies[dec - 1]["health"] -= Damage(enemies[dec - 1]["armor"], stats["weapon"])
                            break
                        else:
                            print("Answer the question, Goblins are approaching!")
                    except:
                        print("Answer the question, Goblins are approaching!")
                    t = t+1
                    if t > 10:
                        print("The goblins beat you to death while you stand their dumbfounded.")
                        return 1
                #enemies[0]["health"] -= Damage(enemies[0]["armor"], stats["weapon"])
                #attack lowest
                break
            else:
                print("Answer the question, Goblins are approaching!")
            t = t+1
            if t > 10:
                print("The goblins beat you to death while you stand their dumbfounded.")
                return 1
            #attack lowest
        for Goblin in enemies:
            if Goblin["health"]<= 0:
                print("You have killed "+Goblin["name"])
                enemies.remove(Goblin)
        for Goblin in enemies:
            print (Goblin["name"]+" is attacking!!")
            stats["health"] -= DamageE(stats["Armor"], Goblin["attack"])
    if stats["health"]<stats["MH"]:
        print("A long rest replenishes your health.")
        stats["health"]=stats["MH"]
    else:
        print("Well that was easy.")
    return 0
def FP0():
    print("Welcome to Cave Explorer 1!\nThe greatest game that is not fun.\nTry not to die.")
    while True:
        decision=input("Are you ready?[y]/n\n")
        if decision.upper() in no:
            print("You want to go play outside, hu?")
            return 13
        elif decision.upper() in yes:
            return 1
def FP1():
    print("Welcome to the Medieval Age.\nYou can go on an adventure or become a farmer.")
    while True:
        decision=input("Would you like to go on an adventure?[y]/n\n")
        if decision.upper() in no:
            print("Farming is a noble job, I think...")
            return 13
        elif decision.upper() in yes:
            return 2
def FP2():
    #shop
    money = 100
    unusedHands = 2
    armor = False
    equipment = []
    options = ['1','2','3','4','5','6','7',]
    Shop = {"1":{"cost":50, "hands":2, "name": "Longsword"},
    "2":{"cost":20, "hands":1, "name": "Short sword"},
    "3":{"cost":40, "hands":2, "name": "Two Short swords"},
    "4":{"cost":40, "hands":0, "name": "Light Armor"},
    "5":{"cost":60, "hands":0, "name": "Heavy Armor"},
    "6":{"cost":20, "hands":1, "name": "Shield"},
    "7":{"cost":40, "hands":2, "name": "Dragon Sword"}}
    print("To go on an adventure you must first buy you equipment.")
    print("============Shop============")
    print("Swords:")
    print("    Long........"+ str(Shop["1"]["cost"]) +" gold [1]")
    print("    Short......."+ str(Shop["2"]["cost"]) +" gold [2]")
    print("    Short, two.."+ str(Shop["3"]["cost"]) +" gold [3]")
    print("Armor:")
    print("    Light......."+ str(Shop["4"]["cost"]) +" gold [4]")
    print("    Heavy......."+ str(Shop["5"]["cost"]) +" gold [5]")
    print("Shield.........."+ str(Shop["6"]["cost"]) +" gold [6]")
    print("To buy something write the number next to the equipment.\nWrite 'leave' to leave,\n'shop' to see the shop again,\nand 'equipment' to see your equipment.")
    while True:
        print("You have "+str(money)+" gold.")
        while True:
            decision=input("Shopkeeper: \"What would you like?\"\n")
            if decision in options:
                break
            elif decision.upper() == "LEAVE":
                if len(equipment) > 1:
                    print("You arrive at a cave, rumored to be filled with loot but defended by Goblins.")

                    # initialize Equepment
                    # this defines the abilities of your Equpment
                    if "6" in equipment:
                        H = 100
                    else:
                        H = 70
                        
                    if "4" in equipment:
                        A = 15
                    elif "5"in equipment:
                        A = 18
                    else:
                        A = 10
                    
                    if "1" in equipment:
                        W = 10
                    elif "2" in equipment:
                        W = 6
                    elif "3" in equipment:
                        W = "Shortswords"
                    elif "7" in equipment:
                        W = 40
                    
                    stats = {"health":H, "Armor": A, "weapon": W,"MH":H}
                    return(3, stats)
                else:
                    print("You need some equipment.")
                
            elif decision.upper() == "SHOP":
                print("============Shop============")
                print("Swords:")
                print("    Long........"+ str(Shop["1"]["cost"]) +" gold [1]")
                print("    Short......."+ str(Shop["2"]["cost"]) +" gold [2]")
                print("    Short, two.."+ str(Shop["3"]["cost"]) +" gold [3]")
                print("Armor:")
                print("    Light......."+ str(Shop["4"]["cost"]) +" gold [4]")
                print("    Heavy......."+ str(Shop["5"]["cost"]) +" gold [5]")
                print("Shield.........."+ str(Shop["6"]["cost"]) +" gold [6]")
            elif decision.upper() == "EQUIPMENT":
                for equip in equipment:
                    print(Shop[equip]["name"], end=" ")
                print("")
            else:
                print("Shopkeeper: \"I don't know that you are asking for.\"")
        if decision in equipment:
            if decision == '7':
                print("Shopkeeper: \"You may not give up the Dragon Sword!\"") 
            else:
                #remove thing
                if decision == '4' or decision == '5':
                    armor = False
                money = money+Shop[decision]["cost"]
                unusedHands = unusedHands+Shop[decision]["hands"]
                equipment.remove(decision)
                print(Shop[decision]["name"]+" was removed.")
        else:
            if decision == '7':
                print("    ~~~~~~~~~~~~~~~~~~")
                print("~~~~~~~~~  ROAR  ~~~~~~~~~")
                print("    ~~~~~~~~~~~~~~~~~~")
                print("Shopkeeper: \"I see you have chosen the Dragon Sword.\"")
                money = money-Shop[decision]["cost"]
                unusedHands = unusedHands-Shop[decision]["hands"]
                equipment.append("7")
                if "1" in equipment:
                    money = money+Shop["1"]["cost"]
                    unusedHands = unusedHands+Shop["1"]["hands"]
                    equipment.remove("1")
                    print(Shop["1"]["name"]+" was removed.")
                elif "2" in equipment:
                    money = money+Shop["2"]["cost"]
                    unusedHands = unusedHands+Shop["2"]["hands"]
                    equipment.remove("2")
                    print(Shop["2"]["name"]+" was removed.")
                elif "3" in equipment:
                    money = money+Shop["3"]["cost"]
                    unusedHands = unusedHands+Shop["3"]["hands"]
                    equipment.remove("3")
                    print(Shop["3"]["name"]+" was removed.")
                elif "6" in equipment:
                    money = money+Shop["6"]["cost"]
                    unusedHands = unusedHands+Shop["6"]["hands"]
                    equipment.remove("6")
                    print(Shop["6"]["name"]+" was removed.")
            elif decision == '2':
                if '3' in equipment:
                    # replace 2 for 3
                    if money+Shop["3"]["cost"] >= Shop[decision]["cost"] and unusedHands+Shop['3']["hands"] >= Shop[decision]["hands"]:
                        money = money-Shop[decision]["cost"] + Shop["3"]["cost"]
                        unusedHands = unusedHands-Shop[decision]["hands"] + Shop["3"]["hands"]
                        equipment.remove("3")
                        equipment.append("2")
                    else:
                        print("You don't have enough money or hands for that.")
                elif "2" in equipment:
                    # sell back 2
                    money = money+Shop[decision]["cost"]
                    unusedHands = unusedHands+Shop[decision]["hands"]
                    equipment.remove(decision)
                    print("something was removed")
                else:
                    # buy 2 without chetch
                    if money >= Shop[decision]["cost"] and unusedHands >= Shop[decision]["hands"]:
                        money = money-Shop[decision]["cost"]
                        unusedHands = unusedHands-Shop[decision]["hands"]
                        equipment.append(decision)
                    elif money >= Shop[decision]["cost"]:
                        print("you do not have enough hands")
                    elif unusedHands >= Shop[decision]["hands"]:
                        print("you do not have enough money")
                    else:
                        print("You do not have enough money and hands for that.")
            elif decision == '3':
                if '2' in equipment:
                    # replace
                    if money+Shop["2"]["cost"] >= Shop[decision]["cost"] and unusedHands+Shop['2']["hands"] >= Shop[decision]["hands"]:
                        money = money-Shop[decision]["cost"] + Shop["2"]["cost"]
                        unusedHands = unusedHands-Shop[decision]["hands"] + Shop["2"]["hands"]
                        equipment.remove("2")
                        equipment.append("3")
                    else:
                        print("You don't have enough money or hands for that.")
                elif "3" in equipment:
                    # sell
                    money = money+Shop[decision]["cost"]
                    unusedHands = unusedHands+Shop[decision]["hands"]
                    equipment.remove(decision)
                    print("something was removed")
                else:
                    # if can't buy
                    if money >= Shop[decision]["cost"] and unusedHands >= Shop[decision]["hands"]:
                        money = money-Shop[decision]["cost"]
                        unusedHands = unusedHands-Shop[decision]["hands"]
                        equipment.append(decision)
                    elif money >= Shop[decision]["cost"]:
                        print("You do not have enough hands.")
                    elif unusedHands >= Shop[decision]["hands"]:
                        print("You do not have enough money.")
                    else:
                        print("You do not have enough money and hands for that.")
            # if can't buy
            elif money < Shop[decision]["cost"] or unusedHands < Shop[decision]["hands"]:
                if money >= Shop[decision]["cost"]:
                    print("You do not have enough hands.")
                elif unusedHands >= Shop[decision]["hands"]:
                    print("You do not have enough money.")
                else:
                    print("You do not have enough money and hands for that.")
            elif decision == '4' or decision == '5' and not armor:
                money = money-Shop[decision]["cost"]
                unusedHands = unusedHands-Shop[decision]["hands"]
                equipment.append(decision)
                armor = True
            elif decision == '4' or decision == '5' and armor:
                print("Shopkeeper: *strange expression* \"You can't wear two kinds of armor.\"")
            elif money >= Shop[decision]["cost"] and unusedHands >= Shop[decision]["hands"]:
                money = money-Shop[decision]["cost"]
                unusedHands = unusedHands-Shop[decision]["hands"]
                equipment.append(decision)
def FP3():
    # entrence to the cave
    while True:
        decision=input("Would you like to enter?[y]/n\n")
        if decision.upper() in yes:
            print("In you go!!")
            print("As you walk through, you see clear signs of goblins, bones, footprints and other garbage.")
            return 4
        elif decision.upper() in no:
            while True:
                decision=input("Would you like to go back to the shop?[y]/n\n")
                if decision.upper() in yes:
                    return 2
                elif decision.upper() in no:
                    while True:
                        decision=input("Would you like to continue the game[y]/n\n")
                        if decision.upper() in yes:
                            pass
                        elif decision.upper() in no:
                            print("You could have at least said so earlier.")
                            return 13
                        else:
                            print("Answer the question!")
                else:
                    print("Answer the question!")
        else:
            print("Answer the question!")

def FP4(goblins10):
    # first fork in the cave
    print("You arrive at a junction. To the left, the cave gets smaller.\nYou therefore can deduce it leads to some sort of storage.")
    while True:
        decision=input("Would you like to turn to the left, continue on straight or exit the cave? L/S/E\n")
        if decision.upper() == "L":
            return 4.1
        elif decision.upper() == "E":
            if goblins10:
                print("Maybe you weren't made for fighting.")
                return 13
            else:
                print("You return victoriously, with gold to last a lifetime (100 gold).")
                return 14
        elif decision.upper() == "S":
            return 5
        else:
            print("Answer the question!")
def FP4_L():
    # left at the junction
    print("As you follow the cave to the left you see more foot prints and bones. The faint sound of goblins is surely to grow to a howler as you walk onward. You are met with another fork.")
    while True:
        decision=input("Would you like to go left, right or back? L/R/B\n")
        if decision.upper() == "L":
            return 4.11
        elif decision.upper() == "R":
            return 4.12
        elif decision.upper() == "B":
            return 4
        else:
            print("Answer the question!")
def FP4_L1(goblins4_L1,stats):
    # left form last left
    print("This part of the cave is almost identical to the rest until you are met with a door.")
    if goblins4_L1:
        print("The sound of Goblins emanates from the other side. As you open it the sound stops as the little creatures turn to face you.\nGoblins!")
        result = fightGoblins(4, stats)
        if result == 0: #   Win
            return (4.1, False)
        elif result == 1: # Death
            return (13, True)
        elif result == 2: # Fleeing
            return 13, True
        else:
            print("Something very bad happened...")
    else:
        print("Nothing is here but the bodies of dead Goblins. You decide to turn arround")
        return (4.1, False)
def FP4_L2(goblins4_L2,stats):
    # right from last left
    print("As you continue down the passage you are met with a door.")
    if goblins4_L2:
        print("You open it a see...\nGoblins!")
        result = fightGoblins(4, stats)
        if result == 0: #   Win
            return 4.1, False
        elif result == 1: # Death
            return 13, True
        elif result == 2: # Fleeing
            return 13, True
        else:
            print("Something very bad happened...")
    print("There is nothing here except the bodies of the goblins you have killed. You turn around")
    return 4.1, False
def FP5():
    # continue on the main passage
    # right to a room with goblins
    # left to continue on the main passage
    print("You know you are walking deep into the hillside, the air\ngets colder, the ground gets muddy, and you can feel the\nslope of the passage. The cave splits. Both sides are nearly identical.")
    while True:
        decision=input("Would you like to go left, right or back?L/R/B\n")
        if decision.upper() == "L":
            return 7
        elif decision.upper() == "R":
            return 6
        elif decision.upper() == "B":
            return 4
        else:
            print("Answer the question!")
def FP6(goblins6,stats):
    # fight gobilns after turning right
    print("As you turn to the right the ground levels out. In the\nfaint torch light you can make out what might just be a door handle. You arrive at it wondering what loot lies inside...")
    if goblins6:
        print("You open the door and seven goblins are waiting in ambush!")
        result=fightGoblins(7, stats)
        if result == 0: #   Win
            return 5, False
        elif result == 1: # Death
            return 13, True
        elif result == 2: # Fleeing
            return 13, True
        else:
            print("Something very bad happened...")
    else:
        print("There is nothing here except the bodies of the goblins you have killed. You go back")
        return 5, False
def FP7():
    # contiune on the main passage and arive at another junction
    # turn left to go to the food storage room
    # strate to the king's chaimbers
    print("After turning to the left,\nthe cave bends to the right, getting muddyer each step.\nYou arrive at a junction. To the left the cave gets smaller.\nIt is likely a storage room.")
    while True:
        decision=input("Would you like to stay the main passage, turn to the left or go back?S/L/B\n")
        if decision.upper() == "L":
            return 8
        elif decision.upper() == "B":
            return 5
        elif decision.upper() == "S":
            if goblins10:
                print("You arrive at a grate door. Your senses predict an imminent boss fight.")
            else:
                print("You walk back toward the door of the King Goblin.")
            return 9
        else:
            print("Answer the question!")
def FP8(goblins8,stats):
    # the food storage woth goblin chefs
    print("You arrive at a room filled with food and drink fit for a king.")
    if goblins8:
        print("A horde of Goblin Chefs Attack you with pots and pans!")
        result=fightGoblins(9, stats)
        if result == 0: #   Win
            print("It seems they were as bad at fighting as they were at cooking.")
            return 7, False
        elif result == 1: # Death
            return 13, True
        elif result == 2: # Fleeing
            return 13, True
        else:
            print("Something very bad happened...")
    else:
        print("You enjoy the Goblin King's food while looking\nover the dead bodies of his minions.")
        return 7, False
def FP9(goblins10):
    # this is the door to the king goblins's room
    # go in if you dear
    if goblins10:
        print("As you open the grate door you feel a sinking feeling in your stomach.")
        decision = input("Are you ready for this fight?[y]/n\n")
        if decision.upper() in yes:
            return 10
        elif decision.upper() in no:
            return 7
        else:
            print("Answer the question!")
    print("You have reached the great door again")
    return 10
# Fight the Goblin King
def FP10(goblins10,stats):
    print("A beautiful room for Goblins.")
    if goblins10:
        print("There are stone pillars and a throne in the back, but no time for admiring, the Goblins King's army advances!")
        result=fightGoblins(9, stats, True)
        if result == 0: #   Win
            print("You are victorious over the Goblin King!")
            while True:
                decision=input("Now that you have conquered the Goblin's cave would you like to return gloriously?[y]/n\n")
                if decision.upper() in yes:
                    return 14, False
                elif decision.upper() in no:
                    return 11, False
                else:
                    print("Answer the question!")
        elif result == 1: # Death
            return 13, True
        elif result == 2: # Fleeing
            return 13, True
        else:
            print("Something very bad happened...")
    else:
        while True:
            decision=input("Would you like to Search the room or go Back?S/B\n")
            if decision.upper() == "S":
                return 11, False
            elif decision.upper() == "B":
                return 7, False
            else:
                print("Answer the question!")
def FP11():
    # decribe the throne room
    print("The great throne that sits in the center of the room does\nnot look goblins, and the stone work is also or high quality.\nIt is most likely the Goblins have taken over someone else's\nlair. There are two doors one on the left and one on the right.")
    while True:
        decision=input("Would you like to go to the room on the left, on the right, or back?L/R/B\n")
        if decision.upper() == "L":
            print("Nothing much here, you decide to go back.")
            return 11
        elif decision.upper() == "R":
            return 12
        elif decision.upper() == "B":
            return 7
        else:
            print("Answer the question!")
def FP12():
    # a room that contains the king's bed
    print("The grand sleeping quarters of the Goblin King. It's nice but it smells.")
    while True:
        decision=input("Would you like to leave?[y]\n")
        if decision.upper() in yes:
            return 10
        elif decision.upper() == "S":
            # you can go to another room from the previous
            print("You have found a secret passage!!\nYou follow it down to find another room. It contains the ~Dragon Sword~.")
            return 10
        else:
            print("Answer the question!")
# this while loop runs the functions so that they are not called in each other
# because this is not posible:
#   def fun1():
#       something
#       fun2()
#   def fun2()
#       something
#       fun1()
# I did this:
while True:
    if position == 0:
        position = FP0()
    elif position == 1:
        position = FP1()
    elif position == 2:
        position,stats = FP2()
    elif position == 3:
        position=FP3()
    
    elif position == 4:
        position=FP4(goblins10)
    elif position == 4.1:
        position=FP4_L()
    elif position == 4.11:
        position,goblins4_L1=FP4_L1(goblins4_L1,stats)
    elif position == 4.12:
        position,goblins4_L2=FP4_L2(goblins4_L2,stats)
    
    elif position == 5:
        position=FP5()
    elif position == 6:
        position,goblins6=FP6(goblins6,stats)
    elif position == 7:
        position=FP7()
    elif position == 8:
        position,goblins8=FP8(goblins8,stats)
    elif position == 9:
        position=FP9(goblins10)
    elif position == 10:
        position,goblins10=FP10(goblins10,stats)
    elif position == 11:
        position=FP11()
    elif position == 12:
        position = FP12()
    elif position == 13:
        print("A for effort, F for achieving honor.")
        break
    else:
        print("You're a winner!")
        break
